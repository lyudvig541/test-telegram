const { Telegraf } = require('telegraf');
const { message } = require('telegraf/filters');

const Token = "6747153303:AAFksKrzlxgXPpafyHRIuPeJhGt8ikDqQ9c";
const bot = new Telegraf(Token);
const web_link  = "https://test-telegram-red.vercel.app/";

bot.command('start' ,(ctx) => {
    try {
        console.log('Received /start command');
        ctx.reply("Welcome :)))))", {
            reply_markup: {
                keyboard: [[{ text: "web app", web_app: { url: web_link } }]],
            },
        })
    } catch (error) {
        console.error('Error in /start command:', error);
    }
});

// Log errors and start the bot
bot.catch((error) => console.error('Telegraf error:', error));
bot.launch().then(() => console.log('Bot started'));
